#ifndef DATA_GRABBING_OBJECT_H
#define DATA_GRABBING_OBJECT_H

#include <QObject>
#include <QVariant>

#include "dataparsernorthern.h"

class DataGrabbingObject : public QObject{
   Q_OBJECT
public:
    explicit DataGrabbingObject (QObject* parent = 0);
    Q_INVOKABLE QVariantList boxNWhiskersDataGrabber(QString year, QString month, int columnNumber, int option);
    Q_INVOKABLE QVariantList polarChartDataGrabber(QString year, QString month, int columnNumber);
    Q_INVOKABLE QVariantList bnw_opcija_1(int year, int month, int column);
    Q_INVOKABLE QVariantList bnw_opcija_2(int year, int month, int column);
    Q_INVOKABLE QVariantList bnw_opcija_3(int year, int month, int column);
    Q_INVOKABLE QVariantList polarChart_opcija_1(int year, int month);
    Q_INVOKABLE QVariantList polarChart_opcija_2(int month);

private:
    DataParserNorthern northern = DataParserNorthern();
    int monthStringToInt(QString month);
};

#endif // DATA_GRABBING_OBJECT_H
