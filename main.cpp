#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QtWidgets/QApplication>
#include <QtQuick/QQuickView>

#include <QQmlContext>

#include "datagrabbingobject.h"

int main(int argc, char *argv[])
{

    QApplication app0(argc, argv);

    QQuickView viewer;

#ifdef Q_OS_WIN
    QString extraImportPath(QStringLiteral("%1/../../../../%2"));
#else
    QString extraImportPath(QStringLiteral("%1/../../../%2"));
#endif
    viewer.engine()->addImportPath(extraImportPath.arg(QGuiApplication::applicationDirPath(),QString::fromLatin1("qml")));

    QObject::connect(viewer.engine(), &QQmlEngine::quit, &viewer, &QWindow::close);

    DataGrabbingObject* dataGrabber = new DataGrabbingObject();
    viewer.engine()->rootContext()->setContextProperty("externalDataGrabber", dataGrabber);

    viewer.setTitle(QStringLiteral("PNP-projekt"));
    viewer.setSource(QUrl("qrc:/main.qml"));
    viewer.setResizeMode(QQuickView::SizeRootObjectToView);
    viewer.show();

    return app0.exec();
}
