#include "dataparsernorthern.h"

DataParserNorthern::DataParserNorthern(QString inputFileName)
    : m_InputFileName(std::move(inputFileName))
{
}

QString DataParserNorthern::getInputFileName()
{
    return m_InputFileName;
}

double DataParserNorthern::readColumnFromInputRowLine(QString& line, short columnIndex)
{
    short numberOfCharsToSkip, numberOfCharsToRead;
    // Podrazumijeva se da columnIndex krece od 0.
    numberOfCharsToRead = m_SizeOfColumns[columnIndex];

    // line = "2005   3   1   9   4     2.050" gdje je HS zadnji stupac
    // potrebno je preskociti= 5 * 4 charova
    // potrebno je za procitati= 10 charova (od kojih su samo zadnjih 5 puni, a prvih 5 su razmaci)

    numberOfCharsToSkip = 0;
    for(auto i=0; i<columnIndex; i++)
        numberOfCharsToSkip += m_SizeOfColumns[i];

    // Ovo vraca String ispod stupca ukljucujuci i whitespace s lijeve strane
    QString columnString = line.mid(numberOfCharsToSkip, numberOfCharsToRead).trimmed();

    return columnString.toDouble();;
}

bool DataParserNorthern::readDataFromInputFile(long rowBegin, long rowEnd)
{
    // Predvidam da za svaki mjesec postoji 31*8 redaka s podacima
    short nubmerOfRowsToRead = 31*8;

    // Na pocetku svakog novog citanja iz datoteke se mapa brise!
    m_mapOfMonthlyStatisticalData.clear();

    std::vector<double> HS, TP, TheH, WS;

    HS.reserve(nubmerOfRowsToRead);
    TP.reserve(nubmerOfRowsToRead);
    TheH.reserve(nubmerOfRowsToRead);
    WS.reserve(nubmerOfRowsToRead);

    int currentYear = 0, currentMonth = 0;
    int startingRowLineOfMonth = 0, endingRowLineOfMonth = 0;

    // Pocinje citanje
    int rowCounter = 0;

    if( rowBegin == -1)
        rowBegin = m_StartingRowNumber;
    if( rowEnd == -1)
        rowEnd = 31*8*12*7;

    QFile data(m_InputFileName);
    if (!data.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        std::cout<< "readDataFromInputFile(): Error reading file!" << std::endl;
        return -1;
    }
    else {
        std::cout<< "readDataFromInputFile(): ALL GOOD reading file!" << std::endl;
        QTextStream in(&data);
        while (!in.atEnd()) {

            QString currentLine = in.readLine();

            if(++rowCounter < rowBegin)
                continue;
            if(rowCounter > rowEnd)
                break;

            double rowData = 0.0;
            rowData = readColumnFromInputRowLine(currentLine, 0);
            int newYear    = static_cast<int>(rowData);
            rowData = readColumnFromInputRowLine(currentLine, 1);
            int newMonth   = static_cast<int>(rowData);

            if( currentYear == 0 && currentMonth == 0)
            {
                currentYear = newYear;
                currentMonth= newMonth;

                // Zapamti na kojoj liniji pocinje mjesec!
                startingRowLineOfMonth = rowCounter;    // Tu je rowCounter = 16
            }
            else if( newMonth != currentMonth)
            {
                // Spremi podatke o pocetnoj i zavrsnoj liniji prethodnog mjeseca, kako bi se mogli pronaci
                if(endingRowLineOfMonth != 0)
                    std::cout << "readDataFromInputFile(): endingRodLineOfMonth != 0" << std::endl;
                endingRowLineOfMonth = rowCounter-1;

                QString yearMonth;
                // Kako bi spremio informacije o odredenim mjesecima i godinama u Mapu, korisitm kljuc formata:
                // year_month, tj. za podatke iz 1 mjeseca 2005. godine => "2005_01"
                if( currentMonth < 10 )
                    yearMonth = QString::number(currentYear) + "_0" + QString::number(currentMonth);
                else
                    yearMonth = QString::number(currentYear) + "_" + QString::number(currentMonth);

                // Treba skrenuti paznju na to da rowCounter krece od 1, a prva linija u dokumentu iz kojeg se cita je na rednom broju 16.
                // Stoga ce prvi mjesec imati pocetak na liniji 16.
                std::array<int, 2> RowLinesOfMonth{};
                RowLinesOfMonth[0] =  startingRowLineOfMonth;
                RowLinesOfMonth[1] =  endingRowLineOfMonth;

                m_mapOfMonthStartingEndingRowNumber.insert(std::make_pair(yearMonth, RowLinesOfMonth));
                startingRowLineOfMonth  = rowCounter;
                endingRowLineOfMonth    = 0;

                // Posalji prikupljene podatke na izracun statistike!
                std::map<QString, std::array<std::vector<double>, 3>> tempStatisticsMap =
                        this->calculateBoxNWhiskersStatistics({HS, TP, WS}, currentYear, currentMonth);

                m_mapOfMonthlyStatisticalData.insert(tempStatisticsMap.begin(), tempStatisticsMap.end());

                std::map<QString, std::vector<std::array<double, 8>>> tempDirectionMap =
                        this->calculatePolarChartStatistics({HS, TheH}, currentYear, currentMonth);

                m_mapOfDirectionStatistics.insert(tempDirectionMap.begin(), tempDirectionMap.end());

                // Nakon sto su podaci za mjesec dana procesuirani, mogu obrisati svoje vektore radi ustede prostora
                HS.clear();
                TP.clear();
                TheH.clear();
                WS.clear();
                currentYear = newYear;
                currentMonth = newMonth;
            }

            rowData = readColumnFromInputRowLine(currentLine, 5);
            HS.push_back(rowData);
            rowData = readColumnFromInputRowLine(currentLine, 6);
            TP.push_back(rowData);
            rowData = readColumnFromInputRowLine(currentLine, 7);
            TheH.push_back(rowData);
            rowData = readColumnFromInputRowLine(currentLine, 8);
            WS.push_back(rowData);

        }
        data.close();
    }

    return true;
}

std::map<QString, std::array<std::vector<double>, 3>> DataParserNorthern::calculateBoxNWhiskersStatistics(std::array<std::vector<double>,3> columns, int year, int month)
{

    QString yearMonth;
    if(month < 10)
        yearMonth = QString::number(year) + "_0" + QString::number(month);
    else
        yearMonth = QString::number(year) + "_" + QString::number(month);

    // ovo je niz od 3 stupca, a svaki stupac ima svoje ekstreme, kvartile, mean i stdDev.
    std::array<std::vector<double>,3> computedStatisticsForColumns;

    for (auto i=0; i<columns.size(); i++)
    {
        double meanValue = 0, standardDeviationValue = 0;
        double lowerExtreme = 0.0, upperExtreme = 0.0, lowerQuartile = 0.0, medianValue = 0.0, upperQuartile = 0.0;

        //0) datumLabel
        // Informaciju o datumu mijenjam iz stringa u double, kako bi se informacija proslijedila koristeći isti vektor
        // u koji zapisujem statistiku. Zbog toga ce mi prvi element u vectoru statistika uvijek biti informacija o datumu
        // Cilj spremanja informacije o datumu je kako bi se datum prikazao na Label-u u grafu
        // UPDATE: Mislim da kasnije uopce ne koristim tu informaciju o datumu...

        yearMonth.replace("_", ".");	// Pretvara "2005_03" u "2005.03"
        double dateLabel = yearMonth.toDouble(); // Pretvara "2005.03" u 2005.03 double tip podataka
            // NAPOMENA: 2005.1 je iz "2005_10", tj. deseti mjesec, a ne prvi!
        computedStatisticsForColumns[i].push_back(dateLabel);

        //1) kvartili
        auto Q1 = columns[i].size() / 4;
        auto Q2 = columns[i].size() / 2;
        auto Q3 = Q1 + Q2;

        // Izracun ekstrema!
        std::nth_element(columns[i].begin(), columns[i].begin(), columns[i].end());
        lowerExtreme = columns[i][0];
        std::nth_element(columns[i].begin(), columns[i].end()-1, columns[i].end());
        upperExtreme = columns[i][columns[i].size()-1];

        // Izracun kvartila!
        std::nth_element(columns[i].begin(),          columns[i].begin() + Q1, columns[i].end());
        std::nth_element(columns[i].begin() + Q1 + 1, columns[i].begin() + Q2, columns[i].end());
        std::nth_element(columns[i].begin() + Q2 + 1, columns[i].begin() + Q3, columns[i].end());

        lowerQuartile = columns[i][Q1];
        medianValue = columns[i][Q2];
        upperQuartile = columns[i][Q3];

        computedStatisticsForColumns[i].push_back(lowerExtreme);
        computedStatisticsForColumns[i].push_back(lowerQuartile);
        computedStatisticsForColumns[i].push_back(medianValue);
        computedStatisticsForColumns[i].push_back(upperQuartile);
        computedStatisticsForColumns[i].push_back(upperExtreme);

        //2) aritmeticka sredina
        meanValue = std::accumulate(std::begin(columns[i]), std::end(columns[i]), 0.0) / (columns[i].size());
        computedStatisticsForColumns[i].push_back(meanValue);

        double sum = 0;
        for(double j : columns[i])
        {
            //3) standardna devijacija
            double squared_difference = j - meanValue;
            sum += squared_difference*squared_difference;
        }
        standardDeviationValue = std::sqrt(sum / columns[i].size());
        computedStatisticsForColumns[i].push_back(standardDeviationValue);
    }

    // Nakon svih izracuna treba spremiti sve podatke u std::Map i poslati ih nazad kao rezultat metode!
    std::map<QString, std::array<std::vector<double>, 3>> mapOfColumnData;
    mapOfColumnData.insert(std::make_pair(yearMonth, computedStatisticsForColumns));

    return mapOfColumnData;
}

std::map<QString, std::vector<std::array<double, 8>> > DataParserNorthern::calculatePolarChartStatistics(std::array<std::vector<double>,2> columns, int year, int month)
{
    // columns[0] je HS, columns[1] je TheH!

    QString yearMonth;
    if(month < 10)
        yearMonth = QString::number(year) + "_0" + QString::number(month);
    else
        yearMonth = QString::number(year) + "_" + QString::number(month);

    // 8 doubleova u svakom od stupaca su smjerovi valova!
    std::map<QString, std::vector<std::array<double, 8>> > result;
    std::vector<std::array<double, 8>> categoryBasedDirectionVector{};

    // Cilj metode je proci istovremeno kroz HS i TheH te na temelju vrijednosti stupca HS podijeliti retke na odreden broj kategorija
    // Zasad imam 5 kategorija: x<1.5, 1.5<=x<3, 3<=x<5.5, 5.5<=x<7, 7<=x
    const uint numberOfCategories = 5;

    // Nakon sto odredim u koju kategoriju spada redak, interpetiram vrijednost u TheH stupcu koja mi daje informaciju o smjeru
    // te inkrementiram brojac za smjer u pitanju za kategoriju u pitanju
    categoryBasedDirectionVector.reserve(numberOfCategories);

    // Ispuni sve arrayeve u vectoru sa pocetnim vrijednostima od 0
    for(auto i=0; i<numberOfCategories; i++)
    {
        std::array<double, 8> someArray;
        someArray.fill(0);
        categoryBasedDirectionVector.push_back(someArray);
    }

    enum waveDirections { N=0, NE, E, SE, S, SW, W, NW };

    struct waveHeightCategories {
        float min = 0;
        float max = 20;
    };

    std::array<waveHeightCategories, numberOfCategories> defaultCategories;
//    defaultCategories[0].min = 0;
    defaultCategories[0].max = 1.5;
    defaultCategories[1].min = 1.5;
    defaultCategories[1].max = 3;
    defaultCategories[2].min = 3;
    defaultCategories[2].max = 5.5;
    defaultCategories[3].min = 5.5;
    defaultCategories[3].max = 7;
    defaultCategories[4].min = 7;
//    defaultCategories[4].max = 20;

    for(auto i=0; i<columns[0].size(); i++)
    {
        // Prodi istovremeno kroz HS i TheH te na temelju vrijednosti stupca HS podijeliti retke na odreden broj kategorija
        // Zasad imam 5 kategorija: x<1.5, 1.5<=x<3, 3<=x<5.5, 5.5<=x<7, 7<=x

        // Rasporedi vrijednost u HS retku u jednu od zadanih kategorija!
        for (auto j=0; j<defaultCategories.size(); j++) {

            //////
            // Oprostite profesore, onaj bolji pristup provjeri smjerova nisam stigao napraviti.
            //////

            if( (columns[0][i] >= defaultCategories[j].min) && ( columns[0][i] < defaultCategories[j].max) )
            {
                // Sada nadi smjer prema vrijednosti iz TheH retka!
                // North
                if( (columns[1][i] >= 337.5 ) && (columns[1][i] <= 360) || (columns[1][i] >= 0 ) && (columns[1][i] < 22.5) )
                    categoryBasedDirectionVector[j][waveDirections::N]++;
                // North East
                else if( (columns[1][i] >= 22.5 ) && (columns[1][i] < 67.5) )
                    categoryBasedDirectionVector[j][waveDirections::NE]++;
                // East
                else if( (columns[1][i] >= 67.5 ) && (columns[1][i] < 112.5) )
                    categoryBasedDirectionVector[j][waveDirections::E]++;
                // South East
                else if( (columns[1][i] >= 112.5 ) && (columns[1][i] < 157.5) )
                    categoryBasedDirectionVector[j][waveDirections::SE]++;
                // South
                else if( (columns[1][i] >= 157.5 ) && (columns[1][i] < 202.5) )
                    categoryBasedDirectionVector[j][waveDirections::S]++;
                // South West
                else if( (columns[1][i] >= 202.5 ) && (columns[1][i] < 247.5) )
                    categoryBasedDirectionVector[j][waveDirections::SW]++;
                // West
                else if( (columns[1][i] >= 247.5 ) && (columns[1][i] < 292.5) )
                    categoryBasedDirectionVector[j][waveDirections::W]++;
                // North West
                else if( (columns[1][i] >= 292.5 ) && (columns[1][i] < 337.5) )
                    categoryBasedDirectionVector[j][waveDirections::NW]++;
            }

        }
    }

    // Ispisuje vrijednosti, za potrebe debugiranja!
//    for(auto i=0; i<categoryBasedDirectionVector.size(); i++)
//        for(auto j=0; j<categoryBasedDirectionVector[0].size(); j++)
//            std::cout<< "categoryBasedDirectionVector[" << i << "][" << j << "]= " << categoryBasedDirectionVector[i][j] <<std::endl;

    result.insert(std::make_pair(yearMonth, categoryBasedDirectionVector));
    return result;
}

void DataParserNorthern::readDataFromInputFileForSpecificMonthAndCalculateStatistics(int month)
{
    // Provjeri je li month vazeci
    if(month < 1 || month > 12)
        std::cout<< "readDataFromInputFileForSpecificMonthAndCalculateStatistics(): Invalid month!" <<std::endl;

    std::vector<std::array<int, 2>> monthStartEndLineVector;

    QString monthWanted;
    if(month < 10)
        monthWanted = "0" + QString::number(month);
    else
        monthWanted =  QString::number(month);

    std::map<QString, std::array<int, 2>>::iterator iterator;
    for(iterator=m_mapOfMonthStartingEndingRowNumber.begin(); iterator!=m_mapOfMonthStartingEndingRowNumber.end(); ++iterator)
    {
        std::string month = iterator->first.toStdString().substr(5,2);
        // Ako vrati 0, vrijednosti su jednake
        if( month != monthWanted.toStdString())
            continue;
//        std::cout<< "rowNo[0,1]= " << iterator->second[0] << ", " << iterator->second[1] <<std::endl;

        monthStartEndLineVector.push_back(iterator->second);
    }

    // Rezerviraj mi dovoljno za 1 mjesec * X godina
    // Ovo ce biti pogresno u svakom fajlu koji ne cita tocno 7 godina! (MAGIC_NUMBER)
    // monthStartEndLineVector.size() mi daje informaciju o broju pojavljivanja odredenog mjeseca u datoteci.
    // Tj., mjesec sijecanj se pojavljuje 7 puta u 8 godina (2006-2012 / 2005-2012)
    short nubmerOfRowsToRead = 31*8 * monthStartEndLineVector.size();

    // Rezervira 1736 doublova po jednom vektoru!!! (MEMORY)
    std::array<std::vector<double>, 3> allColumns;
    // HS je 0, TP je 1, WS je 2
    allColumns[0].reserve(nubmerOfRowsToRead);
    allColumns[1].reserve(nubmerOfRowsToRead);
    allColumns[2].reserve(nubmerOfRowsToRead);

    // Pocinje citanje
    std::array<int, 2> lineLimits;
    int lineLimitPairIndex = 0;
    lineLimits = monthStartEndLineVector[lineLimitPairIndex];

    int rowCounter          = 0;

    QFile data(m_InputFileName);
    if (!data.open(QIODevice::ReadOnly | QIODevice::Text))
        std::cout<< "readDataFromInputFileForSpecificMonthAndCalculateStatistics(): Error reading file!" << std::endl;
    else {
        QTextStream in(&data);
        std::cout<< "data.open()!" <<std::endl;
        while (!in.atEnd())
        {

            QString currentLine = in.readLine();
            if(++rowCounter < lineLimits[0])
            {
//                std::cout<< "-->if(" << rowCounter << "<" << lineLimits[0] << ") = TRUE. Skipping line..." <<std::endl;
                continue;
            }

            double rowData = 0.0;
            rowData = readColumnFromInputRowLine(currentLine, 5);
            allColumns[0].push_back(rowData);
            rowData = readColumnFromInputRowLine(currentLine, 6);
            allColumns[1].push_back(rowData);
            rowData = readColumnFromInputRowLine(currentLine, 8);
            allColumns[2].push_back(rowData);

            if(rowCounter >= lineLimits[1])
            {
//                std::cout<< "-->if(" << rowCounter << ">=" << lineLimits[1] << ") = TRUE. Increment row limits!" <<std::endl;
                if(++lineLimitPairIndex < monthStartEndLineVector.size() )
                    lineLimits = monthStartEndLineVector[lineLimitPairIndex];
                else
                {
                    // Ako je inkrementirani index premasio duzinu vectora, ugasi petlju
//                    std::cout<< "++" << lineLimitPairIndex-1 << "<" << monthStartEndLineVector.size()
//                             << " = FALSE. Breaking parsing loop!" <<std::endl;
                    break;
                }
            }
        }
        data.close();
        std::cout<< "data.close()!" <<std::endl;
        }
    // Ovdje se dobiveni podaci za svaki stupac zajedno salju u metodu za izracun statistike!

    std::array<std::vector<double>, 3> computedStatisticsForColumns;
    computedStatisticsForColumns[0].reserve(8);
    computedStatisticsForColumns[1].reserve(8);
    computedStatisticsForColumns[2].reserve(8);

    for (auto i=0; i<computedStatisticsForColumns.size(); i++)
    {
        // allColumns.size() bi trebal biti 5!
        double meanValue = 0, standardDeviationValue = 0;
        double lowerExtreme = 0.0, upperExtreme = 0.0, lowerQuartile = 0.0, medianValue = 0.0, upperQuartile = 0.0;

//		//0) datumLabel
        // "01" sprema kao 1, "10" kao 10
        computedStatisticsForColumns[i].push_back(monthWanted.toDouble());

        //1) kvartili
        auto Q1 = allColumns[i].size() / 4;
        auto Q2 = allColumns[i].size() / 2;
        auto Q3 = Q1 + Q2;

        // Izracun ekstrema!
        std::nth_element(allColumns[i].begin(), allColumns[i].begin(), allColumns[i].end());
        lowerExtreme = allColumns[i][0];
        std::nth_element(allColumns[i].begin(), allColumns[i].end()-1, allColumns[i].end());
        upperExtreme = allColumns[i][allColumns[i].size()-1];

        // Izracun kvartila!
        std::nth_element(allColumns[i].begin(),          allColumns[i].begin() + Q1, allColumns[i].end());
        std::nth_element(allColumns[i].begin() + Q1 + 1, allColumns[i].begin() + Q2, allColumns[i].end());
        std::nth_element(allColumns[i].begin() + Q2 + 1, allColumns[i].begin() + Q3, allColumns[i].end());

        lowerQuartile = allColumns[i][Q1];
        medianValue = allColumns[i][Q2];
        upperQuartile = allColumns[i][Q3];


        computedStatisticsForColumns[i].push_back(lowerExtreme);
        computedStatisticsForColumns[i].push_back(lowerQuartile);
        computedStatisticsForColumns[i].push_back(medianValue);
        computedStatisticsForColumns[i].push_back(upperQuartile);
        computedStatisticsForColumns[i].push_back(upperExtreme);

        //2) aritmeticka sredina
        meanValue = std::accumulate(std::begin(allColumns[i]), std::end(allColumns[i]), 0.0) / (allColumns[i].size());
        computedStatisticsForColumns[i].push_back(meanValue);

        double sum = 0;
        for(double j : allColumns[i])
        {
            //3) standardna devijacija
            double squared_difference = j - meanValue;
            sum += squared_difference*squared_difference;
        }
        standardDeviationValue = std::sqrt(sum / allColumns[i].size());
        computedStatisticsForColumns[i].push_back(standardDeviationValue);
    }

    // Nakon svih izracuna treba spremiti sve podatke u std::Map i poslati ih nazad kao rezultat metode!
    // Ova mapa ima za kljuc mjesece, a za vrijednost ima statistiku za odredeni mjesec kroz sve godine!
    m_mapOfStatisticalColumnDataForEachMonthThroughAllYears.insert(std::make_pair(monthWanted, computedStatisticsForColumns));
}

std::vector<std::array<std::vector<double>, 3>> DataParserNorthern::getBoxNWhiskersStatisticsForSingleMonthThroughAllYears(int month)
{
    QString monthWanted;
    if(month < 10)
        monthWanted = "0" + QString::number(month);
    else
        monthWanted = QString::number(month);

    std::vector<std::array<std::vector<double>, 3>> statisticsForSpecificMonthThroughAllYears;    // klasicna trodimenzionalna matrica

    std::map<QString, std::array<std::vector<double>, 3>>::iterator iterator;
    for(iterator=m_mapOfMonthlyStatisticalData.begin(); iterator!=m_mapOfMonthlyStatisticalData.end(); ++iterator)
    {
        std::string month = iterator->first.toStdString().substr(5,2);
        // Ako vrati 0, vrijednosti su jednake
        if( month != monthWanted.toStdString())
            continue;
//        std::cout << iterator->first.toStdString().substr(5,2) << std::endl;
        statisticsForSpecificMonthThroughAllYears.push_back(iterator->second);
    }

    return statisticsForSpecificMonthThroughAllYears;
}

std::vector<std::array<std::vector<double>, 3>> DataParserNorthern::getBoxNWhiskersStatisticsForAllMonthsThroughSingleYear(int year)
{
    std::string yearWanted = std::to_string(year);

    std::vector<std::array<std::vector<double>, 3>> statisticsForAllMonthsThroughSingleYear;

    std::map<QString, std::array<std::vector<double>, 3>>::iterator iterator;
    for(iterator=m_mapOfMonthlyStatisticalData.begin(); iterator!=m_mapOfMonthlyStatisticalData.end(); ++iterator)
    {
        std::string year = iterator->first.toStdString().substr(0,4);
        // Ako vrati 0, vrijednosti su jednake
        if( year != yearWanted)
            continue;
        statisticsForAllMonthsThroughSingleYear.push_back(iterator->second);
    }

    return statisticsForAllMonthsThroughSingleYear;
}

std::vector<std::array<std::vector<double>, 3>> DataParserNorthern::getBoxNWhiskersStatisticsForAllMonthsThroughAllYears()
{
    // Provjeriti je li mapa vec popunjena!
    if(m_mapOfStatisticalColumnDataForEachMonthThroughAllYears.empty())
        for(auto i=1; i<13; i++)
            this->readDataFromInputFileForSpecificMonthAndCalculateStatistics(i);

    std::vector<std::array<std::vector<double>, 3>> statisticsForAllMonthsThroughAllYears;    // klasicna trodimenzionalna matrica

    std::map<QString, std::array<std::vector<double>, 3>>::iterator iterator;

    for(iterator=m_mapOfStatisticalColumnDataForEachMonthThroughAllYears.begin(); iterator!=m_mapOfStatisticalColumnDataForEachMonthThroughAllYears.end(); ++iterator)
    {
//        std::cout<< "it->first= " << iterator->first.toStdString() <<std::endl;
//        std::cout<< "\tlE= " << iterator->second[0][0] <<std::endl;
//        std::cout<< "\tq1= " << iterator->second[0][1] <<std::endl;
//        std::cout<< "\tq2= " << iterator->second[0][2] <<std::endl;
//        std::cout<< "\tq3= " << iterator->second[0][3] <<std::endl;
//        std::cout<< "\tuE= " << iterator->second[0][4] <<std::endl;
        statisticsForAllMonthsThroughAllYears.push_back(iterator->second);
    }

    return statisticsForAllMonthsThroughAllYears;
}

std::vector<std::array<double, 8>> DataParserNorthern::getPolarChartStatisticsForSingleMonthThroughSingleYear(int year, int month)
{
    std::cout<< "year= " << year <<std::endl;
    std::cout<< "month= " << month <<std::endl;
    QString yearMonth;
    if(month < 10)
        yearMonth = QString::number(year) + "_0" + QString::number(month);
    else
        yearMonth = QString::number(year) + "_" + QString::number(month);

    std::vector<std::array<double, 8>> result;

    std::map<QString, std::vector<std::array<double, 8>> >::iterator iterator;
    for(iterator=m_mapOfDirectionStatistics.begin(); iterator!=m_mapOfDirectionStatistics.end(); ++iterator)
    {
        std::string date = iterator->first.toStdString();
        if( yearMonth.toStdString() != date)
            continue;
//        std::cout<< "date= " << date <<std::endl;
        for(auto i=0; i<5; i++)
        {
            for(auto j=0; j<8; j++)
            {
//                std::cout<< "vector[" << i << "][" << j << "]=" << iterator->second[i][j] <<std::endl;
                result = iterator->second;
            }
        }
    }

    return result;
}

std::vector<std::vector<std::array<double, 8>> > DataParserNorthern::getPolarChartStatisticsForSingleMonthThroughAllYears(int month)
{
    QString monthWanted;
    if(month < 10)
        monthWanted = "0" + QString::number(month);
    else
        monthWanted = QString::number(month);

    std::vector<std::vector<std::array<double, 8>> > statisticsForSpecificMonthThroughAllYears;

    std::map<QString, std::vector<std::array<double, 8>> >::iterator iterator;
    for(iterator=m_mapOfDirectionStatistics.begin(); iterator!=m_mapOfDirectionStatistics.end(); ++iterator)
    {
        std::string month = iterator->first.toStdString().substr(5,2); // "2001_03" za ozujak
        if( month != monthWanted.toStdString())
            continue;
        statisticsForSpecificMonthThroughAllYears.push_back(iterator->second);
    }

    return statisticsForSpecificMonthThroughAllYears;
}
