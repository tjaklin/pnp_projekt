#include "datagrabbingobject.h"

DataGrabbingObject::DataGrabbingObject(QObject* parent)  : QObject(parent)
{
    // 16 je prvi redak s pravim podacima. Prije toga je header!
    if ( !northern.readDataFromInputFile(16) )
      std::cout<< "MyObject:northern(): ERROR reading data!" <<std::endl;
}

int DataGrabbingObject::monthStringToInt(QString month)
{
    int result = 0;

    if(month == "Jan")
        result = 1;
    else if(month == "Feb")
        result = 2;
    else if(month == "Mar")
        result = 3;
    else if(month == "Apr")
        result = 4;
    else if(month == "May")
        result = 5;
    else if(month == "Jun")
        result = 6;
    else if(month == "Jul")
        result = 7;
    else if(month == "Aug")
        result = 8;
    else if(month == "Sep")
        result = 9;
    else if(month == "Oct")
        result = 10;
    else if(month == "Nov")
        result = 11;
    else if(month == "Dec")
        result = 12;

    return result;
}

Q_INVOKABLE QVariantList DataGrabbingObject::boxNWhiskersDataGrabber(QString year, QString month, int columnNumber, int option)
{
    if(option == 1)
        return this->bnw_opcija_1(year.toInt(), this->monthStringToInt(month), columnNumber);
    else if(option == 2)
        return this->bnw_opcija_2(year.toInt(), this->monthStringToInt(month), columnNumber);
    else
        return this->bnw_opcija_3(year.toInt(), this->monthStringToInt(month), columnNumber);
}

Q_INVOKABLE QVariantList DataGrabbingObject::polarChartDataGrabber(QString year, QString month, int option)
{
    if(option == 1)
        return this->polarChart_opcija_1(year.toInt(), this->monthStringToInt(month));
    else
        return this->polarChart_opcija_2(this->monthStringToInt(month));
}

Q_INVOKABLE QVariantList DataGrabbingObject::bnw_opcija_1(int year, int month, int columnNumber)
{

    std::vector<std::array<std::vector<double>,3>> inputData;
    inputData = northern.getBoxNWhiskersStatisticsForSingleMonthThroughAllYears(month);
    std::cout<< "MyObject:inputData().size(): " << inputData.size() <<std::endl;

    // 7 iteracija podataka jer saljem 1 BoxSet za svaku godinu!
    const uint numberOfIterations = inputData.size();
    // Broj stupaca je fiksan u ovom fajlu! (MAGIC_NUMBER)
    const uint numberOfColumns    = 3;
    // Ovo je fiksno! Za svaki skup podataka (mjesec) se cuva 8 vrijednosti, osim kada se dozivaju podaci za 12 kroz 7 god! (MAGIC_NUMBER)
    const uint numberOfColumnData = 8;

    QVariantList newList;
    newList.push_back(numberOfIterations);

    // prolazi kroz 7 elemenata (svaki za jednu godinu)
    for(auto i=0; i<numberOfIterations; i++)
    {
//        std::cout << "Instance[" << i << "]\n";
        // prolazi kroz 8 elemenata (svaki za jednu statistiku)
        for(auto j=0; j<numberOfColumnData-2; j++)
        {
//            std::cout << "\tColumnData= " << inputData[i][columnNumber][j] << std::endl;
            if(j==0)
                newList.push_back(QString::number(inputData[i][columnNumber][j], 'g', 6));
            else
                newList.push_back(inputData[i][columnNumber][j]);
        }
    }

    return newList;
}

Q_INVOKABLE QVariantList DataGrabbingObject::bnw_opcija_2(int year, int month, int columnNumber)
{
    std::vector<std::array<std::vector<double>,3>> inputData;
    inputData = northern.getBoxNWhiskersStatisticsForAllMonthsThroughSingleYear(year);
    std::cout<< "MyObject:inputData().size(): " << inputData.size() <<std::endl;

    // 12 iteracija podataka jer saljem 1 BoxSet za svaki mjesec!
    const uint numberOfIterations = inputData.size();
    const uint numberOfColumns    = 3;
    const uint numberOfColumnData = 8;

    QVariantList newList;
    newList.push_back(numberOfIterations);

    // prolazi kroz 12 elemenata (svaki za jednu godinu)
    for(auto i=0; i<numberOfIterations; i++)
    {
//        std::cout << "Instance[" << i << "]\n";
        // prolazi kroz 8 elemenata (svaki za jednu statistiku)
        for(auto j=0; j<numberOfColumnData-2; j++)
        {
//            std::cout << "\tColumnData= " << inputData[i][columnNumber][j] << std::endl;
            if(j==0)
                newList.push_back(QString::number(inputData[i][columnNumber][j], 'g', 6));
            else
                newList.push_back(inputData[i][columnNumber][j]);
        }
    }

    return newList;
}

Q_INVOKABLE QVariantList DataGrabbingObject::bnw_opcija_3(int year, int month, int columnNumber)
{
    std::vector<std::array<std::vector<double>,3>> inputData;
    inputData = northern.getBoxNWhiskersStatisticsForAllMonthsThroughAllYears();
    std::cout<< "MyObject:inputData().size(): " << inputData.size() <<std::endl;

    // 12 iteracija podataka jer saljem 1 BoxSet za svaki mjesec!
    const uint numberOfIterations = inputData.size();
    const uint numberOfColumns    = 3;
    const uint numberOfColumnData = 8;

    QVariantList newList;
    newList.push_back(numberOfIterations);

    // Ovo mi se izvrsava 12 puta, za sve mjesece
    for(auto i=0; i<numberOfIterations; i++)
    {
//        std::cout << "Instance[" << i << "]\n";
        // prolazi kroz 5 elemenata (svaki za jednu statistiku unutar boxNWhiskers chart-a)
        // j uvjet mi je (MAGIC_NUMBER, a ne cita zadnja 2 elementa jer se oni ne koriste za prikaz na boxNWhiskers grafu! Ta 2 elementa su mean i stdDev
        for(auto j=0; j<numberOfColumnData-2; j++)
        {
//            std::cout << "\tColumnData= " << inputData[i][columnNumber][j] << std::endl;
            if(j==0)
                newList.push_back(QString::number(inputData[i][columnNumber][j], 'g', 6));
            else
                newList.push_back(inputData[i][columnNumber][j]);
        }
    }

    return newList;
}

Q_INVOKABLE QVariantList DataGrabbingObject::polarChart_opcija_1(int year, int month)
{
    std::vector<std::array<double, 8>> inputData;
    inputData = northern.getPolarChartStatisticsForSingleMonthThroughSingleYear(year, month);

    std::cout<< "inputData.size()= " << inputData.size() <<std::endl;

    // (MAGIC_NUMBER)
    const uint numberOfCategories = inputData.size();

    QVariantList result;
    for(auto i=0; i<numberOfCategories; i++)
        result.push_back(0);


    for(auto i=0; i<numberOfCategories; i++)
    {
        QVariantList newLineSeries;
        for(auto j=0; j<inputData[0].size(); j++)
        {
//            std::cout<< "inputData[" << i << "][" << j << "] = " << inputData[i][j] <<std::endl;
            newLineSeries.append(inputData[i][j]);
        }
        result[i] = newLineSeries;
    }

    return result;
}

Q_INVOKABLE QVariantList DataGrabbingObject::polarChart_opcija_2(int month)
{
    std::vector<std::vector<std::array<double, 8>>> inputData;
    inputData = northern.getPolarChartStatisticsForSingleMonthThroughAllYears(month);

    // newList je 2D lista koja sadrzi sve LineSeries.
    // Duzina newList je jednaka broju kategorija, tj. u ovom slucaju 5 (MAGIC_NUMBER)
    // Zbog toga stvaram 5 elemenata unutar newList koje cu kasnije popuniti sa 5 LineSeries.
    const uint numberOfCategories = 5;

    QVariantList newList;
    for(auto i=0; i<numberOfCategories; i++)
        newList.push_back(0);

    // Za svaku iteraciju mjeseca
    for(auto i=1; i<inputData.size(); i++)
    {
        // Za svaki array element (od 8) u vektoru kategorija
        for(auto j=0; j<inputData[0][0].size(); j++)
        {
            // (MAGIC_NUMBER)
            inputData[0][0][j] += inputData[i][0][j];
            inputData[0][1][j] += inputData[i][1][j];
            inputData[0][2][j] += inputData[i][2][j];
            inputData[0][3][j] += inputData[i][3][j];
            inputData[0][4][j] += inputData[i][4][j];
        }
    }

    // 5 je broj kategorija (MAGIC_NUMBER)
    // Za svaku kategoriju stvori jedan LineSeries
    for(auto i=0; i<numberOfCategories; i++)
    {
        QVariantList newLineSeries;
        for(auto j=0; j<8; j++)
        {
            newLineSeries.push_back(inputData[0][i][j]);
        }
        newList[i] = newLineSeries;
    }

    return newList;
}












