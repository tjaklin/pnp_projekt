import QtQuick 2.12
import QtQuick.Controls 2.12
import QtCharts 2.3

Page {
    id: page
    width: 800
    height: 600
    property alias listViewColumns: listViewColumns
    property alias labelColumns: labelColumns
    property alias comboBoxMonth: comboBoxMonth
    property alias labelMonth: labelMonth
    property alias comboBoxYear: comboBoxYear
    property alias labelYear: labelYear
    property alias boxPlotChart: boxPlotChart
    property alias polarChart: polarChart

    property alias labelBoxNWhiskers: labelBoxNWhiskers
    property alias listViewPolarCharts: listViewPolarCharts
    property alias labelPolarCharts: labelPolarCharts
    property alias listViewBoxNWhiskers: listViewBoxNWhiskers

    function updateBoxPlotSeries(processingOption) {
        // Hide PC, show BnW
        polarChart.visible = false
        boxPlotChart.visible = true

        // Update title based on selected date
        var year = comboBoxYear.currentText
        var month = comboBoxMonth.currentText
        var columnOption = lmColumn.get(listViewColumns.currentIndex).option
        boxPlotChart.title = "Year " + year + ", Month " + month
        boxPlotSeries.clear()

        var newData = externalDataGrabber.boxNWhiskersDataGrabber(year, month, columnOption, processingOption)

        // Nulti element newData 2D array-a mi govori koliko imam boxSetova u jednoj boxLineSeries.
        var numberOfIterations = newData[0]

        // numberOfIterations mi govori hocu li na grafu prikazivati godine ili mjesece
        axisValueY.max = 0
        axisCategoryX.max = numberOfIterations
        axisCategoryX.tickCount = numberOfIterations

        for(var i=0; i<numberOfIterations; i++)
        {
            var boxData = []

            // i*6 -> 6 je broj statistickih podataka koje gledam! (MAGIC_NUMBER)

            // Informaciju o datumu, koja je zapisana u vektor statistika na poziciji nultog elementa, se ovdje
            // cita koristeci i*6 +1 kako bi se datum u svakoj iteraciji dohvatio van petlje za spremanje statistike
            var boxDataLabel = newData[(i*6)+1]

            axisCategoryX.append(boxDataLabel,i+1)

            // Petlja za spremanje statistike
            for(var j=2; j<7; j++)
                boxData[j-2] = newData[(i*6)+j]

            var maxValue = Math.max.apply(null, boxData)
            if(maxValue > axisValueY.max)
            {
                axisValueY.max = maxValue
                console.log("[UPDATE] axisValueY.max= " + axisValueY.max)
            }

            boxPlotSeries.append(boxDataLabel, boxData)
        }
    }

    function updatePolarChartSeries(processingOption) {
        boxPlotChart.visible = false
        polarChart.visible = true
        polarChart.removeAllSeries()

        var parsedData = externalDataGrabber.polarChartDataGrabber(comboBoxYear.currentText, comboBoxMonth.currentText, processingOption)

        var dataToUse = []
        dataToUse = parsedData

        console.log("dataToUse(parsedData)[i].length= " + dataToUse.length)

        polarChart.legend.visible = true
        polarChart.legend.alignment = Qt.AlignTop

        var newLineSeries = []
        // Hardkodirane kategorije (MAGIC_NUMBER)
        var newLineSeriesCategories = ["[x, 1.5]", "[1.5, x, 3]", "[3, x, 5.5]", "[5.5, x, 7]", "[7, x]"]

        for(var i=0; i<dataToUse.length; i++)
        {
            newLineSeries[i] = polarChart.createSeries(ChartView.SeriesTypeLine, newLineSeriesCategories[i], axisAngular, axisRadial);
            var colorManipulator = i*0.3
            newLineSeries[i].color = Qt.rgba(0.2+colorManipulator, 0.5, 0.7, 1.0)

            var angularCurrent = axisAngular.min
            var dataRadial = 0.05
            axisRadial.max = 110.0
            axisRadial.tickCount = 10
            console.log("Math.max.apply(null, dataToUse[i])= " + Math.max.apply(null, dataToUse[i]))

            newLineSeries[i].append(angularCurrent, dataToUse[i][0])
            angularCurrent+=45
            newLineSeries[i].append(angularCurrent, dataToUse[i][1])
            angularCurrent+=45
            newLineSeries[i].append(angularCurrent, dataToUse[i][2])
            angularCurrent+=45
            newLineSeries[i].append(angularCurrent, dataToUse[i][3])
            angularCurrent+=45
            newLineSeries[i].append(angularCurrent, dataToUse[i][4])
            angularCurrent+=45
            newLineSeries[i].append(angularCurrent, dataToUse[i][5])
            angularCurrent+=45
            newLineSeries[i].append(angularCurrent, dataToUse[i][6])
            angularCurrent+=45
            newLineSeries[i].append(angularCurrent, dataToUse[i][7])
            angularCurrent+=45
            newLineSeries[i].append(angularCurrent, dataToUse[i][0])
        }
    }

    Row {
        id: rowLeft
        width: parent.width * 0.25
        height: parent.height
        spacing: 0
        anchors.left: parent.left

        Column {
            id: column
            spacing: 10
            width: parent.width
            height: parent.height

            Label {
                id: labelBoxNWhiskers
                text: qsTr("Box and Whiskers")
                color: "gray"
            }

            ListView {
                id: listViewBoxNWhiskers
                width: parent.width
                height: 120
                keyNavigationWraps: false
                clip: false
                orientation: ListView.Vertical
                model: ListModel {
                    id: lmBoxNWhiskers
                    ListElement {
                        name: "1 Month Through All Years"
                        option: 1
                    }

                    ListElement {
                        name: "12 Months Through Single Year"
                        option: 2
                    }

                    ListElement {
                        name: "12 Months Through All Years"
                        option: 3
                    }
                }
                delegate: Item {
                    width: parent.width
                    height: 40
                    Column {
                        Text {
                            id: delegateTextId
                            text: name
                        }
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked:{
                            listViewBoxNWhiskers.currentIndex = index
                            updateBoxPlotSeries(lmBoxNWhiskers.get(index).option)
                        }
                    }
                }
                highlight: Rectangle {
                    color: 'lightblue'
                }
                focus: true
            }

            Label {
                id: labelPolarCharts
                text: qsTr("Polar Charts")
                color: "gray"
            }

            ListView {
                id: listViewPolarCharts
                width: parent.width
                height: 85
                keyNavigationWraps: false
                clip: false
                orientation: ListView.Vertical
                model: ListModel {
                    id: lmPolarChart
                    ListElement {
                        name: "1 Month Through Single Year"
                        option: 1
                    }

                    ListElement {
                        name: "1 Month Through All Years"
                        option: 2
                    }
                }

                delegate: Item {
                    width: parent.width
                    height: 40
                    Column {
                        Text { text: name }
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            listViewPolarCharts.currentIndex = index
                            updatePolarChartSeries((lmPolarChart.get(index).option))
                        }
                    }
                }
                highlight: Rectangle {
                    color: 'lightblue'
                }
                focus: true
            }

            Label {
                id: labelColumns
                text: qsTr("Columns")
                color: "gray"
            }

            ListView {
                id: listViewColumns
                width: parent.width
                height: 120
                keyNavigationWraps: false
                clip: false
                orientation: ListView.Vertical
                model: ListModel {
                    id: lmColumn
                    ListElement {
                        name: "HS"
                        option: 0
                    }

                    ListElement {
                        name: "TP"
                        option: 1
                    }

                    ListElement {
                        name: "WS"
                        option: 2
                    }
                }

                delegate: Item {
                    width: parent.width
                    height: 40
                    Column {
                        Text { text: name }
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            listViewColumns.currentIndex = index
                        }
                    }
                }
                highlight: Rectangle {
                    color: 'yellow'
                }
                focus: true
            }
        }
    }

    Column {
        id: columnRight
        x: parent.x + parent.width*0.25
        width: parent.width * 0.75
        height: parent.height

        Row {
            id: rowDateHolder
            width: parent.width
            height: parent.height * 0.1
            spacing: 50

            Label {
                id: labelYear
                text: qsTr("Year:")
                font.pointSize: 11
                padding: 10
            }

            ComboBox {
                id: comboBoxYear
                model: ListModel {
                    id: cbYearItems
                    ListElement { text: "2005" }
                    ListElement { text: "2006" }
                    ListElement { text: "2007" }
                    ListElement { text: "2008" }
                    ListElement { text: "2009" }
                    ListElement { text: "2010" }
                    ListElement { text: "2011" }
                    ListElement { text: "2012" }
                }
            }

            Label {
                id: labelMonth
                text: qsTr("Month:")
                font.pointSize: 11
                padding: 10
            }

            ComboBox {
                id: comboBoxMonth
                model: ListModel {
                    id: cbMonthItems
                    ListElement { text: "Jan"}
                    ListElement { text: "Feb"}
                    ListElement { text: "Mar"}
                    ListElement { text: "Apr"}
                    ListElement { text: "May"}
                    ListElement { text: "Jun"}
                    ListElement { text: "Jul"}
                    ListElement { text: "Aug"}
                    ListElement { text: "Sep"}
                    ListElement { text: "Oct"}
                    ListElement { text: "Nov"}
                    ListElement { text: "Dec"}
                }
            }
        }

        Row {
            id: rowChartHolder
            y: parent.height * 0.1
            width: parent.width
            height: parent.height * 0.9

            ChartView {
                id: boxPlotChart
                title: "2005, Mar"
                width: parent.width
                height: parent.height
                legend.alignment: Qt.AlignBottom
                antialiasing: true

                CategoryAxis {
                    id: axisCategoryX
                    min: 0
                    max: 7
                    tickCount: 7
                }

                ValueAxis {
                    id: axisValueY
                    min: 0.0
                    max: 0.25
                    tickCount: 10
                }

                BoxPlotSeries {
                    id: boxPlotSeries
                    axisX: axisCategoryX
                    axisY: axisValueY
                }
            }

            PolarChartView {
                id: polarChart
                width: parent.width
                height: parent.height
                title: "2005, Mar"
                legend.visible: false
                antialiasing: true

                ValueAxis {
                    id: axisAngular
                    min: 0
                    max: 360
                    tickCount: 9
                }

                ValueAxis {
                    id: axisRadial
                    min: 0.0
                    max: 0.25
                    tickCount: 10
                }
            }
        }
    }
}
