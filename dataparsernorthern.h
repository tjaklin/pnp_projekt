#ifndef DATAPARSERNORTHERN_H
#define DATAPARSERNORTHERN_H

#include <fstream>
#include <sstream>
#include <iostream>

#include <QString>
#include <QFile>
#include <QTextStream>
#include <QCoreApplication>

#include <string>
#include <array>
#include <vector>
#include <map>

#include <algorithm>
#include <iterator>
#include <numeric>
#include <cmath>

class DataParserNorthern
{

private:
    QString m_InputFileName;

    // numberOfColumns se mora saznati citajuci InputFile!
    static const int m_NumberOfColumns  = 10;
    static const int m_StartingRowNumber= 16;
    // sizeOfColumns u broju charova se mora saznati citajuci InputFile!
    const int m_SizeOfColumns[m_NumberOfColumns] = {4, 4, 4, 4, 4, 10, 10, 10, 10, 10};

    // Cuva statisticke podatke za svaki od 3 stupaca koja se koriste u BoxNWhiskers grafu (HS, TP, WS), za svaki pojedini mjesec
    std::map<QString, std::array<std::vector<double>, 3>> m_mapOfMonthlyStatisticalData;
    // Cuva informacije o retcima u inputFile na kojima pocinje i zavrsava svaki mjesec
    std::map<QString, std::array<int,2>> m_mapOfMonthStartingEndingRowNumber;
    // Cuva statisticke podatke za svaki od 3 stupaca koja se koriste u BoxNWhiskers grafu (HS, TP, WS), za odredeni mjesec kroz sve godine
    std::map<QString, std::array<std::vector<double>, 3>> m_mapOfStatisticalColumnDataForEachMonthThroughAllYears;
    // Cuva statisticke podatke za 1 stupac koji se koristi u PolarChart grafu (TheH)
    std::map<QString, std::vector<std::array<double, 8>>> m_mapOfDirectionStatistics;


    // Metode:
    double readColumnFromInputRowLine(QString& line, short columnIndex);

    std::map<QString, std::array<std::vector<double>, 3>> calculateBoxNWhiskersStatistics(std::array<std::vector<double>,3> columns, int year, int month);
    std::map<QString, std::vector<std::array<double, 8>> > calculatePolarChartStatistics(std::array<std::vector<double>,2> columns, int year, int month);

public:

    // Potrebna je apsolutna putanja do datoteke!!
    DataParserNorthern(QString inputFileName= "/home/tibs/Desktop/pnp_MojKod/ostali_podaci/data_neptune/002.50E062.00N_Northern_Sea.ts");

    QString getInputFileName();


    bool readDataFromInputFile(long rowBegin=-1, long rowEnd=-1);

    // ova metoda ispod se ne smije zvati prije newReadFromInputFile()!
    void readDataFromInputFileForSpecificMonthAndCalculateStatistics(int month);

    // Sljedece metode dohvacaju podatke za stupce koji se prikazuju u BoxNWhiskers grafu!
        // ova metoda dohvaca statisticke podatke za odredeni mjesec kroz sve godine
    std::vector<std::array<std::vector<double>, 3>> getBoxNWhiskersStatisticsForSingleMonthThroughAllYears(int month);
        // ova metoda dohvaca statisticke podatke za 12 mjeseci u odredenoj godini
    std::vector<std::array<std::vector<double>, 3>> getBoxNWhiskersStatisticsForAllMonthsThroughSingleYear(int year);
        // ova metoda dohvaca statisticke podatke za 12 mjeseci kroz sve godine
    std::vector<std::array<std::vector<double>, 3>> getBoxNWhiskersStatisticsForAllMonthsThroughAllYears();

    // Sljedece metode dohvacaju podatke za stupce koji se prikazuju u PolarChart grafu!
        // ova metoda dohvaca statisticke podatke iz mape o smjeru valova, za odredeni mjesec u odredenoj godini
    std::vector<std::array<double, 8>> getPolarChartStatisticsForSingleMonthThroughSingleYear(int year, int month);
        // ova metoda dohvaca statisticke podatke iz mape o smjeru valova, za odredeni mjesec kroz sve godine
    std::vector<std::vector<std::array<double, 8>> > getPolarChartStatisticsForSingleMonthThroughAllYears(int month);
};

#endif // DATAPARSERNORTHERN_H
